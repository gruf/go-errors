package errors_test

import (
	"crypto/x509"
	"fmt"
	"io/fs"
	"os"
	"testing"

	"codeberg.org/gruf/go-errors/v2"
)

func TestAs(t *testing.T) {
	var errT errorT
	var errP *fs.PathError
	var p *poser
	_, errF := os.Open("non-existing")
	poserErr := &poser{"oh no", nil}
	testCases := []struct {
		err    error
		target error
		check  func(err, target error) bool
		match  bool
	}{{
		nil,
		errP,
		func(err, target error) bool {
			check := errors.AsV2[*fs.PathError](err)
			return check != nil
		},
		false,
	}, {
		wrapped{"pitied the fool", errorT{"T"}},
		errT,
		func(err, target error) bool {
			check := errors.AsV2[errorT](err)
			return check != errorT{}
		},
		true,
	}, {
		errF,
		errP,
		func(err, target error) bool {
			check := errors.AsV2[*fs.PathError](err)
			return check != nil
		},
		true,
	}, {
		errorT{},
		errP,
		func(err, target error) bool {
			check := errors.AsV2[*fs.PathError](err)
			return check != nil
		},
		false,
	}, {
		wrapped{"wrapped", nil},
		&errT,
		func(err, target error) bool {
			check := errors.AsV2[*errorT](err)
			return check != nil
		},
		false,
	}, {
		&poser{"error", nil},
		&errT,
		func(err, target error) bool {
			check := errors.AsV2[errorT](err)
			return check != errorT{}
		},
		true,
	}, {
		poserErr,
		p,
		func(err, target error) bool {
			check := errors.AsV2[*poser](err)
			return check != nil
		},
		true,
	}, {
		fmt.Errorf("text: %w", &x509.UnknownAuthorityError{}),
		(*x509.UnknownAuthorityError)(nil),
		func(err, target error) bool {
			check := errors.AsV2[*x509.UnknownAuthorityError](err)
			return check != nil
		},
		true,
	}, {
		fmt.Errorf("text: %w", &x509.CertificateInvalidError{}),
		(*x509.UnknownAuthorityError)(nil),
		func(err, target error) bool {
			check := errors.AsV2[*x509.UnknownAuthorityError](err)
			return check != nil
		},
		false,
	}}
	for i, tc := range testCases {
		name := fmt.Sprintf("%d:(As(Errorf(..., %v)) == %#v)", i, tc.err, tc.target)
		// Clear the target pointer, in case it was set in a previous test.
		t.Run(name, func(t *testing.T) {
			if tc.check(tc.err, tc.target) != tc.match {
				t.Fail()
			}
		})
	}
}

type poser struct {
	msg string

	f func(error) bool
}

var poserPathErr = &fs.PathError{Op: "poser"}

func (p *poser) Error() string { return p.msg }

func (p *poser) Is(err error) bool { return p.f != nil && p.f(err) }

func (p *poser) As(err interface{}) bool {
	switch x := err.(type) {
	case **poser:
		*x = p

	case *errorT:
		*x = errorT{"poser"}

	case **fs.PathError:
		*x = poserPathErr

	default:
		return false
	}

	return true
}

type errorT struct{ s string }

func (e errorT) Error() string { return fmt.Sprintf("errorT(%s)", e.s) }

type wrapped struct {
	msg string

	err error
}

func (e wrapped) Error() string { return e.msg }

func (e wrapped) Unwrap() error { return e.err }

type errorUncomparable struct {
	f []string
}

func (errorUncomparable) Error() string {
	return "uncomparable error"
}

func (errorUncomparable) Is(target error) bool {
	_, ok := target.(errorUncomparable)

	return ok
}
