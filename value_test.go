package errors_test

import (
	"testing"

	"codeberg.org/gruf/go-errors/v2"
)

func TestErrorValue(t *testing.T) {
	err := errors.New("base error")

	err = errors.WithValue(err, "hello", "world")

	err = errors.WithValue(err, "float", 1.0)

	err = errors.WithValue(err, "int", 1)

	err2 := errors.New("second error")
	err = errors.WithValue(err, "error", err2)

	err = errors.Join(
		err,
		err2,
	)

	switch {
	case errors.Value(err, "hello") != "world":
		t.Error("failed storing string in error")

	case errors.Value(err, "float") != 1.0:
		t.Error("failed storing float in error")

	case errors.Value(err, "int") != 1:
		t.Error("failed storing int in error")

	case errors.Value(err, "error") != err2:
		t.Error("failed storing error in error")
	}
}
