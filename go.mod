module codeberg.org/gruf/go-errors/v2

go 1.19

require codeberg.org/gruf/go-bitutil v1.0.0

require codeberg.org/gruf/go-bytes v1.0.2 // indirect
