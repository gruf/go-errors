package errors_test

import (
	"fmt"
	"runtime"
	"sync"
	"testing"
	_ "unsafe"

	"codeberg.org/gruf/go-errors/v2"
)

var msgTests = []string{
	"hello world",
	"oh no!",
	"aw shit",
}

var formatTests = []struct {
	fmt string
	arg []interface{}
	exp string
}{
	{
		fmt: "wrap: %v",
		arg: []interface{}{errors.New("error")},
		exp: fmt.Sprintf("wrap: %v", errors.New("error")),
	},
	{
		fmt: "%d %d %d",
		arg: []interface{}{0, 1, 2},
		exp: fmt.Sprintf("%d %d %d", 0, 1, 2),
	},
	{
		fmt: "%#v %#v",
		arg: []interface{}{sync.Mutex{}, map[string]string{"hello": "world"}},
		exp: fmt.Sprintf("%#v %#v", sync.Mutex{}, map[string]string{"hello": "world"}),
	},
}

func TestNew(t *testing.T) {
	for _, msg := range msgTests {
		exp := msg
		err := errors.New(msg)

		if errors.IncludesCaller {
			exp = "v2_test.TestNew " + exp
		}

		if str := err.Error(); str != exp {
			t.Errorf("unexpected error string: expect=%q actual=%q", exp, str)
		}
	}
}

func TestNewf(t *testing.T) {
	for _, test := range formatTests {
		exp := test.exp
		err := errors.Newf(test.fmt, test.arg...)

		if errors.IncludesCaller {
			exp = "v2_test.TestNewf " + exp
		}

		if str := err.Error(); str != exp {
			t.Errorf("unexpected error string: expect=%q actual=%q", exp, str)
		}
	}
}

func TestWrap(t *testing.T) {
	base := errors.New("base error")

	for _, msg := range msgTests {
		exp := msg + ": " + base.Error()
		err := errors.Wrap(base, msg)

		if errors.IncludesCaller {
			exp = "v2_test.TestWrap " + exp
		}

		if str := err.Error(); str != exp {
			t.Errorf("unexpected error string: expect=%q actual=%q", exp, str)
		}

		if !errors.Is(err, base) {
			t.Errorf("wrapped error did not identify as base")
		}
	}
}

func TestWrapf(t *testing.T) {
	base := errors.New("base error")

	for _, test := range formatTests {
		exp := test.exp + ": " + base.Error()
		err := errors.Wrapf(base, test.fmt, test.arg...)

		if errors.IncludesCaller {
			exp = "v2_test.TestWrapf " + exp
		}

		if str := err.Error(); str != exp {
			t.Errorf("unexpected error string: expect=%q actual=%q", exp, str)
		}

		if !errors.Is(err, base) {
			t.Errorf("wrapped error did not identify as base")
		}
	}
}

//go:linkname funcName codeberg.org/gruf/go-errors/v2.funcName
func funcName(fn *runtime.Func) string

type baseerror struct{}

func (baseerror) Error() string { return "base error" }
